package com.example.joke_reader;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.flurry.android.FlurryAgent;

public class BlackList extends SherlockActivity {
	
	private static final int SAVE_ID = 1;
	public static ArrayList<Post> categories = new ArrayList<Post>();
	
	LinearLayout mainView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		setContentView(R.layout.activity_black_list);
		
		mainView = (LinearLayout)findViewById(R.id.scrollviewLinearLayout);
		
		setProgressBarIndeterminateVisibility(true);
		new RetreiveCategories(this, mainView).execute();
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		FlurryAgent.onStartSession(this, "8N29ZTC3YPMQCJ92655R");
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		FlurryAgent.onEndSession(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		menu.add(0, SAVE_ID, 0, "Save").setIcon(R.drawable.abs__ic_cab_done_holo_light).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int itemID = item.getItemId();
		switch( itemID ){
		
		case SAVE_ID:
			System.out.println("Save clicked");
			updateBlacklist();
			finish();
			break;
		}
		
		return true;
	}
	
	private void updateBlacklist(){
		
		MainActivity.blacklist.clear();
		for(int i = 0; i < mainView.getChildCount(); i++){
			
			View v = mainView.getChildAt(i);
			if(v.getClass() == CheckBox.class){
				
				CheckBox chkBox = (CheckBox)v;
				if( chkBox.isChecked() )
					addToBlacklist( (Post)chkBox.getTag() );
			}
		}
		
		MainActivity.blacklist.savePostList();
	}
	
	private void addToBlacklist(Post category){
		
		MainActivity.blacklist.addJoke(category.id);
	}

}
