package com.example.joke_reader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class XmlHelper {
	
	public static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
	
	public static ArrayList<Post> getJokePosts(String result){
		
		InputStream is = new ByteArrayInputStream(result.getBytes());
    	ArrayList<Post> posts = new ArrayList<Post>();
    	String res = "";
    	String id = "";
    	
		try{
    		
    		XmlPullParser parser = Xml.newPullParser();
    		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    		parser.setInput(is, null);
    		parser.nextTag();
    		
    		while (parser.next() != XmlPullParser.END_DOCUMENT) {
    	        
    			if (parser.getEventType() != XmlPullParser.START_TAG)
    	            continue;
    			
    	        String name = parser.getName();
    	        // Starts by looking for the entry tag
    	        if (name.equals("page")) {
    	            
    	        	res = parser.getAttributeValue(null, "title");
    	        	id = parser.getAttributeValue(null, "id");
    	        	
    	        	Post p = new Post();
    	        	p.id = Integer.parseInt(id);
    	        	p.title = res;
    	        	
    	        	if( MainActivity.viewedJokes.inPostList(p.id) )
    	        		
    	        		MainActivity.jokeHandler.downloadJoke();
    	        	else
    	        		posts.add(p);
    	        	
    	        }
    	    }  
    	
    	}catch(Exception e){
    		
    		//is.close();
			
    	}
		
		return posts;
	}
	
public static ArrayList<Post> getJokeCategories(String result){
		
		InputStream is = new ByteArrayInputStream(result.getBytes());
    	ArrayList<Post> posts = new ArrayList<Post>();
    	String res = "";
    	String id = "";
    	
		try{
    		
    		XmlPullParser parser = Xml.newPullParser();
    		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    		parser.setInput(is, null);
    		parser.nextTag();
    		
    		while (parser.next() != XmlPullParser.END_DOCUMENT) {
    	        
    			if (parser.getEventType() != XmlPullParser.START_TAG)
    	            continue;
    			
    	        String name = parser.getName();
    	        // Starts by looking for the entry tag
    	        if (name.equals("page")) {
    	            
    	        	res = parser.getAttributeValue(null, "title");
    	        	id = parser.getAttributeValue(null, "pageid");
    	        	
    	        	Post p = new Post();
    	        	p.id = Integer.parseInt(id);
    	        	p.title = res;
    	        		
    	        	posts.add(p);
    	        	
    	        }
    	    }  
    	
    	}catch(Exception e){
    		
    		//is.close();
			
    	}
		
		return posts;
	}

}
