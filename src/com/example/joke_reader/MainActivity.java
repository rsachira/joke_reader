package com.example.joke_reader;

import java.util.Stack;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.flurry.android.FlurryAdListener;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.FlurryAdType;
import com.flurry.android.FlurryAds;
import com.flurry.android.FlurryAgent;

public class MainActivity extends SherlockActivity implements FlurryAdListener {
	
	public static final String SAVEFILE_NAME = "joke_cache_file";
	public static final String BLACKLIST_FILE = "blacklist";
	public static final String VIEWED_FILE = "viewed";
	public static final String FAVORITE_POST_ID = "favorite_id";
	public static final String FAVORITE_POST_TITLE = "favorite_title";
	
	public static final int SETTINGS_ID = 49;
	public static final int FAVORITES_ID = 59;
	public static final int FAVORITE_ACTIVITY = 67;
	public static final int FAV_RETURN_FAILURE = 77;
	
	public static Stack<Joke> jokeList = new Stack<Joke>();
	public static boolean displayingJoke = false;
	public static Joke currentJoke = new Joke();
	
	public static PostList blacklist;
	public static PostList viewedJokes;
	public static FavoriteList favorites;
	
	public static JokeHandler jokeHandler;
	public static Button randomBtn;
	public static ToggleButton favoriteBtn;
	
	private FrameLayout mBanner;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	/* Display splash screen */
    	
    	Intent splashIntent = new Intent(this, SplashScreen.class);
    	startActivity(splashIntent);
    	
    	setTheme(R.style.Theme_Sherlock_Light);
    	
    	/* Display splash screen */
        
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        
        setContentView(R.layout.activity_main);
        
        Typeface tf = Typeface.createFromAsset(getAssets(), "DhanikaSETT.ttf");
        
        TextView view = (TextView)findViewById(R.id.textView1);
        TextView titleView = (TextView)findViewById(R.id.titleView);
        randomBtn = (Button)findViewById(R.id.button1);
        favoriteBtn = (ToggleButton)findViewById(R.id.imageButton1);
        
        randomBtn.setEnabled(false);
        view.setTypeface(tf);
        view.setMovementMethod(new ScrollingMovementMethod());
        titleView.setTypeface(tf);
        
        jokeHandler = new JokeHandler(this, view, titleView);
        blacklist = new PostList(this, BLACKLIST_FILE);
        viewedJokes = new PostList(this, VIEWED_FILE);
        favorites = new FavoriteList(this);
        
        mBanner = (FrameLayout)findViewById(R.id.framlayout);
        FlurryAds.setAdListener(this);
        
        //dummyFunc();
        
        /*// Testing
        Intent myIntent = new Intent(this, Favorites.class);
        startActivityForResult(myIntent, FAVORITE_ACTIVITY);*/
        
        setProgressBarIndeterminateVisibility(true);
        loadCache();
        
        /*	For testing purposes only
        jokeList = dummyFunc();
        jokeHandler.displayJoke();
        */
    }
    
    @Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	
    	FlurryAgent.setVersionName("splash");
    	FlurryAgent.onStartSession(this, "8N29ZTC3YPMQCJ92655R");
    	
    	FlurryAds.fetchAd(this, "bottom_banner", mBanner, FlurryAdSize.BANNER_BOTTOM);
    }


    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
    	
    	menu.add(0, SETTINGS_ID, 1, "Settings").setIcon(R.drawable.ic_menu_settings).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    	menu.add(0, FAVORITES_ID, 0, "Favorites").setIcon(R.drawable.ic_menu_favorites).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    	return true;
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int itemID = item.getItemId();
		switch( itemID ){
		
		case SETTINGS_ID:
			System.out.println("settings clicked");
			Intent myIntent = new Intent(this, BlackList.class);
	        startActivity(myIntent);
			break;
			
		case FAVORITES_ID:
			System.out.println("favorites clicked");
			Intent favIntent = new Intent(this, Favorites.class);
	        startActivityForResult(favIntent, FAVORITE_ACTIVITY);
		}
		
		return true;
	}
    
    @Override
    protected void onStop() {
    	
    	super.onStop();
    	
    	String data = "";
    	
    	for(int i = 0; i < jokeList.size(); i++){
    		
    		if(i != 0)
    			data += "~~";
    		
    		Joke j = jokeList.get(i);
    		data += Uri.encode( Integer.toString(j.id) ) + "<>" + Uri.encode(j.title) + "<>" + Uri.encode(j.body) + "<>" + Boolean.toString(j.adultJoke);
    	}
    	
    	FileHandler.write(this, SAVEFILE_NAME, data);
    	
    	FlurryAgent.onEndSession(this);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);
    	
    	if(resultCode != Activity.RESULT_OK)
    		return;
    	
    	switch(requestCode){
    	
    	case FAVORITE_ACTIVITY:
    		
    		randomBtn.setEnabled(false);
    		setProgressBarIndeterminateVisibility(true);
    		
    		int favID = data.getIntExtra(FAVORITE_POST_ID, FAV_RETURN_FAILURE);
    		String favTitle = data.getStringExtra(FAVORITE_POST_TITLE);
    		
    		if(favID != FAV_RETURN_FAILURE){
    			
    			Post p = new Post();
    			p.id = favID;
    			p.title = favTitle;
    			
    			new RetreiveFavorite(p).execute();
    		}
    		
    		break;
    	}
    	
    }
    
    public void displayRandomJoke(View view){
    	
    	viewedJokes.addJoke(currentJoke.id);
    	
    	System.out.println("display random joke");
    	jokeHandler.displayJoke();
    	
    	if(jokeList.size() < 3){
    		
    		jokeHandler.downloadJoke();
    	}
    	
    	viewedJokes.savePostList();
    }
    
    public void addToFavorites(View view){
    	
    	if(currentJoke.isFavorite)
    		favorites.removeFavorite(currentJoke.id);
    	else
    		favorites.addToFavorites(currentJoke);
    	
    	favorites.saveFavorites();
    }
    
    private void loadCache(){
    	
    	String data = FileHandler.read(this, SAVEFILE_NAME);
    	
    	if( data.equals("") ){	// No file or nothing in file
    		
    		if( !WebService.isNetworkConnected(this) ){
    			
    			randomBtn.setEnabled(true);
    			setProgressBarIndeterminateVisibility(false);
    			return;
    		}
    		
    		new RetreiveFeedTask(3).execute();
    		return;
    	}
    	//data = Uri.encode(data);
    	//data = "asdasdas~~asdasdad";
    	String joke[] = data.split("~~");
    	
    	for(int i = 0; i < joke.length; i++){
    		
    		String jokeArray[] = joke[i].split("<>");
    		
    		Joke j = new Joke();
    		j.id = Integer.parseInt( Uri.decode(jokeArray[0]) );
    		j.title = Uri.decode(jokeArray[1]);
    		j.body = Uri.decode(jokeArray[2]);
    		j.adultJoke = Boolean.parseBoolean(jokeArray[3]);
    		
    		MainActivity.jokeList.add(j);
    	}
    	
    	MainActivity.jokeHandler.displayJoke();
    }
    
    private void dummyFunc(){
    	
    	Joke j1 = new Joke();
    	j1.title = "11111111111111111";
    	j1.id = 1;
    	
    	Joke j2 = new Joke();
    	j2.title = "22222222222222222";
    	j2.id = 2;
    	
    	Joke j3 = new Joke();
    	j3.title = "33333333333333333";
    	j3.id = 3;
    	
    	Joke j4 = new Joke();
    	j4.title = "444444444444444444";
    	j4.id = 4;
    	
    	favorites.addToFavorites(j1);
    	favorites.addToFavorites(j2);
    	favorites.addToFavorites(j3);
    	favorites.addToFavorites(j4);
    }

	@Override
	public void onAdClicked(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAdClosed(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAdOpened(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onApplicationExit(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRenderFailed(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onVideoCompleted(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean shouldDisplayAd(String arg0, FlurryAdType arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void spaceDidFailToReceiveAd(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void spaceDidReceiveAd(String arg0) {
		// TODO Auto-generated method stub
		FlurryAds.displayAd(this, arg0, mBanner);
	}
    
}






















