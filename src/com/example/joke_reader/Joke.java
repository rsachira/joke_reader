package com.example.joke_reader;

public class Joke {
	
	public int id;
	public String title;
	public String body;
	public boolean adultJoke;
	public boolean isFavorite;
}
