package com.example.joke_reader;

import java.util.ArrayList;

import android.content.Context;

public class PostList {
	
	private static final String DELIMITER = "\n";
	
	private ArrayList<Integer> posts = new ArrayList<Integer>();
	private Context context;
	private String POSTLIST_FILE;
	
	public PostList(Context c, String filename){
		
		context = c;
		POSTLIST_FILE = filename;
		
		String data = FileHandler.read(context, POSTLIST_FILE);
		
		if( data.equals("") )
			return;
		
		String postIDs[] = data.split(DELIMITER);
		
		for(int i = 0; i < postIDs.length; i++)
			posts.add( Integer.parseInt(postIDs[i]) );
	}
	
	public void addJoke(int id){
		
		posts.add(id);
	}
	
	public void savePostList(){
		
		String data = "";
		
		for(int i = 0; i < posts.size(); i++){
			
			if(i != 0)
				data += '\n';
			
			data += Integer.toString( posts.get(i) );
		}
		
		FileHandler.write(context, POSTLIST_FILE, data);
	}
	
	public boolean inPostList(int id){
		
		if( posts.contains(id) )
			return true;
		
		return false;
	}
	
	public void clear(){
		
		posts.clear();
	}
	
}
