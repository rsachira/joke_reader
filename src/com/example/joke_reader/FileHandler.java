package com.example.joke_reader;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import android.content.Context;

public class FileHandler {
	
	
	public static void write(Context context, String filename, String data){
		
		FileOutputStream outputStream;

		try {
		  
			outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
			outputStream.write(data.getBytes());
			outputStream.close();
			System.out.println("stream closed");
			
		} catch (Exception e) {
		  
			e.printStackTrace();
		}
		
	}
	
	public static String read(Context context, String filename){
		
		String data = "";
		
		try{
		
			FileInputStream fis = context.openFileInput(filename);
			data = WebService.getText(fis);
			fis.close();
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
		
		return data;
	}
	
}
