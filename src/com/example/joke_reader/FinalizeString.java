/*

Copyright Protected (c) Sachira Kuruppu 2013 all rights reserved.

The downloaded joke is received. Parse the received xml and extract the joke. Run Trans.UnitoUni on it to get the finalized joke.
Finalized joke is received at onPostExecute().

In onPostExecute() finalized joke is added to the jokeList ( arraylist in MainActivity )

*/

package com.example.joke_reader;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;

import android.os.AsyncTask;
import android.text.Html;
import android.util.Xml;

public class FinalizeString extends AsyncTask<Void, Void, String>  {
	
	private String result;
	private String title;
	private int jokeID;
	private boolean isAdultJoke = false;
	private boolean isFavorite;
	
	public FinalizeString(String joke, String jokeTitle, int id, boolean isAdultJoke, boolean isFavorite){
		
		result = joke;
		title = jokeTitle;
		jokeID = id;
		this.isAdultJoke = isAdultJoke;
		this.isFavorite = isFavorite;
	}
	
	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		InputStream is = new ByteArrayInputStream(this.result.getBytes());
    	String joke = "";
    	
    	try{
    		
    		XmlPullParser parser = Xml.newPullParser();
    		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    		parser.setInput(is, null);
    		parser.nextTag();
    		
    		while (parser.next() != XmlPullParser.END_TAG) {
    	        
    			if (parser.getEventType() != XmlPullParser.START_TAG)
    	            continue;
    			
    	        String name = parser.getName();
    	        // Starts by looking for the entry tag
    	        if (name.equals("text")) {
    	            
    	        	joke = XmlHelper.readText(parser);
    	        	
    	        }
    	    }  
    	
    	}catch(Exception e){
    		
    		//is.close();
			
    	}
    	
    	String finalizedJoke = Trans.UnitoUni(joke);
		
		return finalizedJoke;
	}
	
	@Override
	protected void onPostExecute(String finalizedJoke) {
		
		String jokeBody = Html.fromHtml(finalizedJoke).toString();
		
		if( jokeBody.equals("") )
			return;
		
		Joke joke = new Joke();
		joke.body = jokeBody;
		joke.title = this.title;
		joke.id = this.jokeID;
		joke.adultJoke = isAdultJoke;
		joke.isFavorite = isFavorite;
		
		if(isFavorite){
			
			MainActivity.jokeList.empty();
			MainActivity.displayingJoke = false;
		}
		
		MainActivity.jokeList.push(joke);
		
		if(!MainActivity.displayingJoke)
			MainActivity.jokeHandler.displayJoke();
		
	}
	
}


















