package com.example.joke_reader;

import java.io.InputStream;
import java.util.ArrayList;

import android.os.AsyncTask;

class RetreiveFeedTask extends AsyncTask<Void, Void, Void> {
	
	private ArrayList<Post> jokePosts = new ArrayList<Post>();
	private int num_jokes;	// number of jokes to download
	
	public RetreiveFeedTask(int jokes){
		
		num_jokes = jokes;
	}
	
    protected Void doInBackground(Void... params) {
        
    	InputStream is = null;
        String url = WebService.getRandomPosts(num_jokes); //"http://jokewiki-anarcxh.rhcloud.com/mediawiki/api.php?action=parse&page=නම&format=xml";
        
        is = WebService.getData(url);
        String result = WebService.getText(is);
    	
        jokePosts = XmlHelper.getJokePosts( result );
        
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
    	
    	for(int i = 0; i < jokePosts.size(); i++)
    		
    		new CheckBlacklisted( jokePosts.get(i) ).execute();
    }
    
    
}
