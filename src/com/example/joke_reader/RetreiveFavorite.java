package com.example.joke_reader;

import java.io.InputStream;

import android.os.AsyncTask;

public class RetreiveFavorite extends AsyncTask<Void, Void, Void> {
	
	int jokeID;
	String joke;
	String jokeTitle;
	
	public RetreiveFavorite(Post p){
		
		jokeID = p.id;
		jokeTitle = p.title;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		
		String url = "http://jokewiki-anarcxh.rhcloud.com/mediawiki/api.php?action=parse&pageid="+ jokeID + "&format=xml";
		
		InputStream is = WebService.getData(url);
		joke = WebService.getText(is);
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		
		new FinalizeString(joke, jokeTitle, jokeID, false, true).execute();
	}
	
}
