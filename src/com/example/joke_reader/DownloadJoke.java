/*

Copyright Protected (c) Sachira Kuruppu 2013 all rights reserved.

This asynchronous task downloads the joke given the title. At the conclusion of the task send the given downloaded joke
to FinalizeString to be finalized.

*/

package com.example.joke_reader;

import java.io.InputStream;

import android.os.AsyncTask;

public class DownloadJoke extends AsyncTask<Void, Void, Void> {
	
	private int id;
	private String title;
	private String joke;
	private boolean isAdultJoke = false;
	
	public DownloadJoke(Post jokePost, boolean isAdultJoke){
		
		id = jokePost.id;
		title = jokePost.title;
		this.isAdultJoke = isAdultJoke;
	}
	
	@Override
	protected Void doInBackground(Void... arg0) {
		
		String url = WebService.getJokeUrl(title);
		
		InputStream is = WebService.getData(url);
		joke = WebService.getText(is);
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		new FinalizeString(joke, title, id, isAdultJoke, false).execute();
	}
	
}
