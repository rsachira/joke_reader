package com.example.joke_reader;

import java.util.ArrayList;

import android.content.Context;

public class FavoriteList {

	private static final String FAVORITES_FILE = "joke_favorites";
	private static final String DELIMITER = "\n";
	private static final String INTERNAL_DELIMITER = " ";
	
	private ArrayList<Post> favorites = new ArrayList<Post>();
	private Context context;
	
	public FavoriteList(Context c){
		
		context = c;
		
		String data = FileHandler.read(context, FAVORITES_FILE);
		generateArray(data);
	}
	
	private void generateArray(String data){
		
		if( data.equals("") )
			return;
		
		String posts[] = data.split(DELIMITER);
		
		for(int i = 0; i < posts.length; i++){
			
			String postData[] = posts[i].split(INTERNAL_DELIMITER);
			
			Post p = new Post();
			p.id = Integer.parseInt(postData[0]);
			p.title = postData[1];
			
			favorites.add(p);
		}
		
	}
	
	public void saveFavorites(){
		
		String data = "";
		for(int i = 0; i < favorites.size(); i++){
			
			if(i != 0)
				data += DELIMITER;
			
			Post p = favorites.get(i);
			data += Integer.toString(p.id) + INTERNAL_DELIMITER + p.title;
		}
		
		FileHandler.write(context, FAVORITES_FILE, data);
	}
	
	public void addToFavorites(Joke joke){
		
		Post p = new Post();
		p.id = joke.id;
		p.title = joke.title;
		
		favorites.add(p);
	}
	
	public int getSize(){
		
		return favorites.size();
	}
	
	public Post getFavorite(int index){
		
		return favorites.get(index);
	}
	
	public void removeFavorite(int jokeID){
		
		for(int i = 0; i < favorites.size(); i++){
			
			Post p = favorites.get(i);
			
			if(p.id == jokeID)
				favorites.remove(i);
			
		}
		
	}
	
}
