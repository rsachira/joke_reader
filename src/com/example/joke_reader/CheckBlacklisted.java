package com.example.joke_reader;

import java.io.InputStream;
import java.util.ArrayList;

import android.os.AsyncTask;

public class CheckBlacklisted extends AsyncTask<Void, Void, Void> {
	
	private static final int ADULTJOKE_ID = 293;
	
	private ArrayList<Post> jokeCats = new ArrayList<Post>();
	private Post jokePost;
	private boolean isAdultJoke = false;
	
	public CheckBlacklisted(Post post){
		
		jokePost = post;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		
		String url = "http://jokewiki-anarcxh.rhcloud.com/mediawiki/api.php?action=query&generator=categories&format=xml&titles=" + jokePost.title + "&prop=info";
		InputStream is = WebService.getData(url);
		String data = WebService.getText(is);
		
		jokeCats = XmlHelper.getJokeCategories(data);
		
		return null;
	}
	
	private boolean blacklisted(){	// returns true if blacklisted
		
		for(int i = 0; i < jokeCats.size(); i++){
			
			if(jokeCats.get(i).id == ADULTJOKE_ID)
				isAdultJoke = true;
			
			if( MainActivity.blacklist.inPostList(jokeCats.get(i).id) )
				return true;
		}
		
		return false;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		if( blacklisted() )
			MainActivity.jokeHandler.downloadJoke();
		else
			new DownloadJoke(jokePost, isAdultJoke).execute();
	}
	
}
