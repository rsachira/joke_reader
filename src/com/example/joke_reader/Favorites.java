package com.example.joke_reader;

import com.actionbarsherlock.app.SherlockActivity;
import com.flurry.android.FlurryAgent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class Favorites extends SherlockActivity {
	
	private Typeface tf;
	private LinearLayout ll;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorites);
		
		tf = Typeface.createFromAsset(getAssets(), "DhanikaSETT.ttf");
		ll = (LinearLayout)findViewById(R.id.scrollviewLinearLayout);
		
		for(int i = 0; i < MainActivity.favorites.getSize(); i++){
			
			Post fav = MainActivity.favorites.getFavorite(i);
			displayFavorite(fav, i);
		}
		
		if(MainActivity.favorites.getSize() == 0){
			
			LayoutParams lparams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			
			TextView t = new TextView(this);
			t.setLayoutParams(lparams);
			t.setGravity(Gravity.CENTER);
			t.setText("No Favorites");
			
			ll.addView(t);
		}
		
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		FlurryAgent.onStartSession(this, "8N29ZTC3YPMQCJ92655R");
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		FlurryAgent.onEndSession(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	
	private void displayFavorite(Post fav, int i){
		
		int padding = (int)getResources().getDimension(R.dimen.padding);
		int height = (int)getResources().getDimension(R.dimen.height);
		float fontSize = getResources().getDimension(R.dimen.fontSize);
		
		if( fav.title == null || fav.title.equals("") )
			return;
		
		LayoutParams lparams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height);
		
		TextView t = new TextView(this);
		t.setLayoutParams(lparams);
		t.setGravity(Gravity.CENTER_VERTICAL);
		t.setPadding(padding, padding, padding, padding);
		t.setTypeface(tf);
		t.setText(fav.title);
		t.setTag(fav);
		t.setTextSize(fontSize);
		
		if( ( (i + 1) % 2) == 0 )
			t.setBackgroundResource( R.drawable.textview_border );
		
		t.setClickable(true);
		t.setOnClickListener(new OnClickListener(){
			   public void onClick(View v) {
				   
				   Post p = (Post)v.getTag();
				   Intent resultIntent = new Intent();
				   resultIntent.putExtra(MainActivity.FAVORITE_POST_ID, p.id);
				   resultIntent.putExtra(MainActivity.FAVORITE_POST_TITLE, p.title);
				   setResult(Activity.RESULT_OK, resultIntent);
				   finish();
			   }
			});
		
		ll.addView(t);
	}

}
