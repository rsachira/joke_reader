/*

Copyright Protected (c) Sachira Kuruppu 2013 all rights reserved.

Contains methods to display jokes when necessary

*/

package com.example.joke_reader;

import com.example.joke_reader.R.drawable;

import android.app.Activity;
import android.content.res.Resources;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class JokeHandler {
	
	private Activity activity;
	private TextView view;
	private TextView titleView;
	
	public JokeHandler(Activity parent_activity, TextView bodyView, TextView titleView){
		
		activity = parent_activity;
		view = bodyView;
		this.titleView = titleView;
	}
	
	public void displayJoke(){
		
		activity.setProgressBarIndeterminateVisibility(true);
		
		if( MainActivity.jokeList.empty() ){
			
			if( !WebService.isNetworkConnected(activity) ){
    			
    			MainActivity.randomBtn.setEnabled(true);
    			activity.setProgressBarIndeterminateVisibility(false);
    			return;
    		}
			
			
			MainActivity.displayingJoke = false;
			MainActivity.randomBtn.setEnabled(false);
			return;
		}
		
		Joke joke = MainActivity.jokeList.pop();
		view.setText(joke.body);
		titleView.setText( Html.fromHtml("<u>" + joke.title + "</u>") );
		
		activity.setProgressBarIndeterminateVisibility(false);
		MainActivity.randomBtn.setEnabled(true);
		
		MainActivity.displayingJoke = true;
		MainActivity.currentJoke = joke;
		
		view.scrollTo(0, 0);
		
		if(joke.adultJoke){
			
			view.setVisibility(view.GONE);
			Button clickToView = new Button(activity);
			clickToView.setText("Click Here To View");
			
			clickToView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					view.setVisibility(view.VISIBLE);
					( (LinearLayout)activity.findViewById(R.id.linearlayout) ).removeView(v);
				}
			});
			
			LinearLayout lnr = (LinearLayout)activity.findViewById(R.id.linearlayout);
			lnr.addView(clickToView);
		}
		
		if(joke.isFavorite)
			MainActivity.favoriteBtn.setChecked(true);
		else
			MainActivity.favoriteBtn.setChecked(false);
	}
	
	public void downloadJoke(){
		
		new RetreiveFeedTask(1).execute();
	}
	
	
	
}
