package com.example.joke_reader;

import java.io.InputStream;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ToggleButton;
import android.widget.LinearLayout.LayoutParams;

public class RetreiveCategories extends AsyncTask<Void, Void, Void> {

	private static final String allCategoriesURL = "http://jokewiki-anarcxh.rhcloud.com/mediawiki/api.php?action=query&prop=categories&generator=allcategories&format=xml";
	private LinearLayout mainView;
	private Activity activity;
	
	public RetreiveCategories(Activity a, LinearLayout scrollview){
		
		activity = a;
		mainView = scrollview;
	}
	
	@Override
	protected Void doInBackground(Void... arg0) {
		
		InputStream is = WebService.getData(allCategoriesURL);
		String data = WebService.getText(is);
		
		BlackList.categories = XmlHelper.getJokeCategories(data);
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		Typeface tf = Typeface.createFromAsset(activity.getAssets(), "DhanikaSETT.ttf");
		int height = (int)activity.getResources().getDimension(R.dimen.height);
		float fontSize = activity.getResources().getDimension(R.dimen.fontSize);
		
		LayoutParams lparams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height);
		
		for(int i = 0; i < BlackList.categories.size(); i++){
			
			Post p = BlackList.categories.get(i);
			CheckBox cat = new CheckBox(activity);
			cat.setLayoutParams(lparams);
			cat.setTag(p);
			cat.setTypeface(tf);
			cat.setText( p.title.replace("Category:", "") );
			cat.setTextSize(fontSize);
			
			if( ( (i + 1) % 2) == 0 )
				cat.setBackgroundResource( R.drawable.textview_border );
			
			if( MainActivity.blacklist.inPostList(p.id) )
				cat.setChecked(true);
			
			mainView.addView(cat);
		}
		
		activity.setProgressBarIndeterminateVisibility(false);
	}
	
}
